## I'm a QA Engineer. 
## My CV
[Link to my CV](https://gitlab.com/userinaya231/profile/-/blob/master/CV/y.b-cv.pdf)

[Link to My autotests](https://gitlab.com/userinaya231/puma_autotests)

### Language and Tools
![Header](https://img.shields.io/badge/GitLab-090909?style=for-the-badge&logo=gitlab&logoColor=3ad07d)
![Header](https://img.shields.io/badge/Jira-090909?style=for-the-badge&logo=jira&logoColor=136be1)
![Header](https://img.shields.io/badge/Postman-090909?style=for-the-badge&logo=postman&logoColor=f76935)
![Header](https://img.shields.io/badge/Github-090909?style=for-the-badge&logo=github&logoColor=8cc4d7)
![Header](https://img.shields.io/badge/AzureDevops-090909?style=for-the-badge&logo=azuredevops&logoColor=0074d0)
![Header](https://img.shields.io/badge/Figma-090909?style=for-the-badge&logo=figma&logoColor=7d5fa6)
![Header](https://img.shields.io/badge/MySQL-090909?style=for-the-badge&logo=mysql&logoColor=00618a)
![Header](https://img.shields.io/badge/DevTools-090909?style=for-the-badge&logo=googlechrome&logoColor=2674f2)
![Header](https://img.shields.io/badge/AndroidStudio-090909?style=for-the-badge&logo=androidstudio&logoColor=3ad07d)
![Header](https://img.shields.io/badge/Qase-090909?style=for-the-badge&logo=appqase&logoColor=3ad07d)
![Header](https://img.shields.io/badge/Python-090909?style=for-the-badge&logo=python&logoColor=3ad07d)
![Header](https://img.shields.io/badge/Selenium-090909?style=for-the-badge&logo=selenium&logoColor=3ad07d)
<!-- ![Header](https://img.shields.io/badge/Swagger-090909?style=for-the-badge&logo=swagger&logoColor=7ede2b) -->